package studio.phillip.espressouitest.data.source

import studio.phillip.espressouitest.data.Movie

interface MoviesDataSource {

    fun getMovie(movieId: Int): Movie?
}