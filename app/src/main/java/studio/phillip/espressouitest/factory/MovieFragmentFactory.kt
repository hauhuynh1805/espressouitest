package studio.phillip.espressouitest.factory

import androidx.fragment.app.FragmentFactory
import studio.phillip.espressouitest.ui.movie.DirectorsFragment
import studio.phillip.espressouitest.ui.movie.MovieDetailFragment
import studio.phillip.espressouitest.ui.movie.StarActorsFragment

class MovieFragmentFactory : FragmentFactory(){

    private val TAG: String = "AppDebug"

    override fun instantiate(classLoader: ClassLoader, className: String) =

        when(className){

            MovieDetailFragment::class.java.name -> {
                MovieDetailFragment()
            }

            DirectorsFragment::class.java.name -> {
                DirectorsFragment()
            }

            StarActorsFragment::class.java.name -> {
                StarActorsFragment()
            }

            else -> {
                super.instantiate(classLoader, className)
            }
        }


}

