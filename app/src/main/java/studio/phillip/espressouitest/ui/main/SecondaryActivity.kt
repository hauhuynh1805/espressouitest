package studio.phillip.espressouitest.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_secondary_simple.*
import studio.phillip.espressouitest.R

class SecondaryActivity  : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_secondary_simple)

        button_back.setOnClickListener {
            onBackPressed()
        }
    }
}