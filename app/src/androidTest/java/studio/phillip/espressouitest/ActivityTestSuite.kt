package studio.phillip.espressouitest

import org.junit.runner.RunWith
import org.junit.runners.Suite
import studio.phillip.espressouitest.ui.main.SecondaryActivityTest

@RunWith(Suite::class)
@Suite.SuiteClasses(
    MainActivityTest::class,
    SecondaryActivityTest::class
)
class ActivityTestSuite